﻿using System;
using System.Data;
using System.Linq;
using System.Text;

namespace Calculator
{
    public static class Calculator
    {
        public static double DoOperation(StringBuilder expression)
        {
            var e = expression.ToString();

            try
            {
                return Convert.ToDouble(new DataTable().Compute(e, null));
            }
            catch (Exception ex)
            {
                return double.NaN;
            }
        }

        public static bool CheckOperator(string op)
        {
            var operators = new string[] { "+", "-", "*", "/" };
            var cleanOp = op.TrimStart().TrimEnd();
            if (operators.Any(x => x == cleanOp))
                return true;
            return false;
        }
    }
}
